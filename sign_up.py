#! Python 3 

class Credentials():


    def register(self):   #function 1 Registration
        import shelve
        self.database = shelve.open("Database")
        while True:
            print('Choose a username')
            self.usr = input()
            self.usr = self.usr.lower()

        # check if user is in database

            if self.usr in self.database:
                print('user already exists')
                continue
            else:
                break;


        while True:

            print('Choose a password')
            self.pswd = input()
            print('Retype password')
            self.pswd2 = input()

            if self.pswd != self.pswd2:
                print('passwords are not the same\n')
                continue

            print('registration complete!\n')
            break

        # store the username in a "database"
        self.database[self.usr] = self.pswd
        self.database.close()


    def login(self): # function 2 login
        import shelve
        self.database = shelve.open("Database")

        while True:
            print('What is your username?')
            self.username = input()
            self.username = self.username.lower()

            try:
                if self.username not in self.database.keys():
                    print("Sorry i don't know you")
                    continue

            except AttributeError:
                print('you need to sign up first!')
                break;

            print('What is your password?')
            self.password = input()

            if self.password != self.database[self.username]:
                print('That password is incorrect!')
                continue
            else:
                print('Acces granted')
                print('Welcome ' + self.username)
                break


    def remove(self): # remove user with this function

        import shelve
        self.database = shelve.open("Database")

        while True:
            print("wich user do you want to remove?")
            self.rm_usr = input()

            if self.rm_usr not in self.database.keys():
                print("this user does not exist")
                continue
            else:
                del self.database[self.rm_usr]
                print(f'{self.rm_usr} removed')
                break;

        self.database.close()
